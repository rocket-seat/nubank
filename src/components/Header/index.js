import React, { Component } from 'react'
import { Container, Top, Logo, Title } from './styles'

import logo from '~/assets/logo.png'
import Icon from 'react-native-vector-icons/MaterialIcons'

class Header extends Component {
    render() {
        return (
            <Container>
                <Top>
                    <Logo source={logo} />
                    <Title>Botega</Title>
                </Top>

                <Icon name="keyboard-arrow-down" size={20} color="#fff" />
            </Container>
        )
    }
}

export default Header
