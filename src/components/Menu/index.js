import React, { Component } from 'react'
import QRCode from 'react-native-qrcode'

import Icon from 'react-native-vector-icons/MaterialIcons'
import { Container, Code, Nav, NavItem, NavText, SignOutButton, SignOutButtonText } from './styles'

class Menu extends Component {
    render() {
        return (
            <Container style={{
                opacity: this.props.translateY.interpolate({
                    inputRange: [0, 150],
                    outputRange: [0, 1]
                })
            }}>
                <Code>
                    <QRCode value="https://www.google.com.br" size={80} fgColor="#fff" bgColor="#8b10ae" />
                </Code>

                <Nav>
                    <NavItem>
                        <Icon name="help-outline" size={20} color="#fff" />
                        <NavText>Me ajuda</NavText>
                    </NavItem>
                    <NavItem>
                        <Icon name="person-outline" size={20} color="#fff" />
                        <NavText>Perfil</NavText>
                    </NavItem>
                    <NavItem>
                        <Icon name="credit-card" size={20} color="#fff" />
                        <NavText>Configurar cartão</NavText>
                    </NavItem>
                    <NavItem>
                        <Icon name="smartphone" size={20} color="#fff" />
                        <NavText>Configurações do app</NavText>
                    </NavItem>
                </Nav>

                <SignOutButton onPress={() => {}}>
                    <SignOutButtonText>Sair do app</SignOutButtonText>
                </SignOutButton>
            </Container>
        )
    }
}

export default Menu
